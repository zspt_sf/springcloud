# springcloud

#### 介绍
这是一个SpringCloud微服务小案例

#### 使用

* 先把项目克隆到本地
`git clone https://gitee.com/zspt_sf/springcloud.git`

* 更多详细 参考我的文章 [https://blog.csdn.net/qq_42783654/article/details/110680072](https://blog.csdn.net/qq_42783654/article/details/110680072)
